﻿using System;

namespace PIEDRA_PAPEL_TIJERAS
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hola");
            juego();
        }

        static void juego()
        {
            string resp = "";
            int usuario = 0;
            int comp = 0;

            Console.WriteLine("Juguemos Piedra Papel o Tijeras");

            while (resp != "NO")
            {
                Console.WriteLine("Selecciona una opcion:\n1 para PIEDRA\n2 para PAPEL\n3 para TIJERAS");
                string[] opciones = new string[3] { "PIEDRA", "PAPEL", "TIJERAS" };
                Random rnd = new Random();
                int n = rnd.Next(0, 3);
                Console.WriteLine("Idique su selección:");
                string user = Console.ReadLine().ToUpper();
                Console.WriteLine("Computer:" + opciones[n]);

                if ((user == "PIEDRA"||user =="1") && opciones[n] == "TIJERAS")
                {
                    Console.WriteLine("Usted gana");
                    usuario ++;
                }
                else if ((user == "PIEDRA"||user =="1") && opciones[n] == "PAPEL")
                {
                    Console.WriteLine("Computer wins");
                    comp ++;
                }
                else if ((user == "PAPEL"||user =="2") && opciones[n] == "PIEDRA")
                {
                    Console.WriteLine("Usted gana");
                    usuario ++;
                }
                else if ((user == "PAPEL"||user =="2") && opciones[n] == "TIJERAS")
                {
                    Console.WriteLine("Computadora gana");
                    comp ++;
                }
                else if ((user == "TIJERAS"||user == "3") && opciones[n] == "PIEDRA")
                {
                    Console.WriteLine("Computadora gana");
                    comp ++;
                }
                else if ((user == "TIJERAS"||user == "3") && opciones[n] == "PAPEL")
                {
                    Console.WriteLine("Usuario Gana");
                    usuario ++;
                }
                else
                {
                    Console.WriteLine("Iguales");
                }
                Console.WriteLine("Otra ves? (Si/No):");
                resp = Console.ReadLine().ToUpper();
                Console.WriteLine("------");
            }
            Console.WriteLine("Usted ha ganado " + usuario + " veces");
            Console.WriteLine("La computadora ha ganado " + comp + " veces");
        }
    }
}





